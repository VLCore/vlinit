#!/bin/sh
# description: NFS server.
#
# This is an init script for the knfsd NFS daemons.
# To use NFS, you must first set up /etc/exports.
# See exports(5) for information on /etc/exports format.
#
# Written for Slackware Linux by Patrick J. Volkerding <volkerdi@slackware.com>.

. /etc/rc.d/functions

nfsd_start() {
  # Sanity checks.  Exit if there's no /etc/exports, or if there aren't any
  # shares defined in it.
  if [ ! -r /etc/exports ]; then # no config file, exit:
    return 1
  elif fgrep '/' /etc/exports 1> /dev/null 2> /dev/null ; then
    true # there are directories listed in /etc/exports, continue:
  else # no shares listed in /etc/exports, exit:
    return 1
  fi

  # First, make sure the nfsd kernel module is loaded.  You can comment this
  # part out if you've built nfsd support directly into the kernel.
  if [ -z "`/sbin/lsmod | grep "^nfsd "`" ]; then
    /sbin/modprobe nfsd
  fi

  # If rpc.portmap is not running, start it:
  if ! /usr/sbin/rpcinfo -p 1> /dev/null 2> /dev/null ; then
    if [ -x /sbin/rpc.portmap ]; then
      # echo -n "Starting RPC portmapper"
      /sbin/rpc.portmap
    fi
  fi

  # echo -n "Starting NFS services:"
  if [ -x /usr/sbin/exportfs ]; then
    echo "  /usr/sbin/exportfs -r"
    /usr/sbin/exportfs -r
  fi

  if [ -x /usr/sbin/rpc.rquotad ]; then
    echo "  /usr/sbin/rpc.rquotad"
    /usr/sbin/rpc.rquotad
  fi

  # Start 8 nfsd servers by default (an old Sun standard):
  if [ -x /usr/sbin/rpc.nfsd ]; then
    echo "  /usr/sbin/rpc.nfsd 8"
    /usr/sbin/rpc.nfsd 8
  fi

  if [ -x /usr/sbin/rpc.mountd ]; then
    echo "  /usr/sbin/rpc.mountd"
    /usr/sbin/rpc.mountd
  fi

  # NFS file locking services.  These are optional but recommended.
 
  # With newer kernels, this starts by itself, but this won't hurt:
  if [ -x /usr/sbin/rpc.lockd ]; then
    echo "  /usr/sbin/rpc.lockd"
    /usr/sbin/rpc.lockd
  fi

  if [ -x /usr/sbin/rpc.statd ]; then
    echo "  /usr/sbin/rpc.statd"
    /usr/sbin/rpc.statd
  fi
}

nfsd_stop() {
  killall lockd 2> /dev/null
  killall rpc.statd 2> /dev/null
  killall rpc.mountd 2> /dev/null
  killall nfsd 2> /dev/null
  sleep 1
  killall -9 nfsd 2> /dev/null # make sure :)
  killall rpc.rquotad 2> /dev/null
  /usr/sbin/exportfs -au 2> /dev/null
}

nfsd_restart() {
  nfsd_stop
  sleep 1
  nfsd_start
}

case "$1" in
start)
  nfsd_start
  ;;
stop)
  nfsd_stop
  ;;
restart)
  nfsd_restart
  ;;
reload)
  ;;
status)
  echo mounted NFS
  mount -l -t nfs
  ;;
*)
  echo "usage $0 start|stop|restart|reload|status"
esac
